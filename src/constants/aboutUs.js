import PAYMENTS from '../assets/image/vector/payments.svg';
import ASSISTANCE from '../assets/image/vector/assistance.svg';
import OWNER from '../assets/image/vector/owner.svg';
import DELIVERY from '../assets/image/vector/delivery.svg';
import { Image, Text } from '@chakra-ui/react';
import React from 'react';

const Important = ({ label, color }) => (
    <Text color={color} fontWeight="900" letterSpacing=".5px" fontSize={{ base: "3xl", lg: "5xl" }}>
        {label}
    </Text>
);

const Welcome = () => {
    return (
        <>
            Hazlos tuyos.
            <Important label="Consulta gratuita." color="#febe30" /> Solo en
            Knowledge.
        </>
    );
};

const Assistance = () => {
    return (
        <>
            <Important label="Datos reales" color="#007aff" /> de los mejores
            especialistas.
        </>
    );
};

const Information = () => {
    return (
        <>
            Precios <Important label="sin realizar contacto" color="#ff0053" />
            con terceros
        </>
    );
};

const Security = () => {
    return (
        <>
            Excelentes formas de pagar.{' '}
            <Important label="Compra con confianza." color="#68cc45" />
        </>
    );
};

const aboutUs = [
    {
        color: '#007aff',
        label: 'Asistencia',
        description: <Assistance />,
        avatar: <Image htmlWidth="48px" src={ASSISTANCE} />,
    },

    {
        color: '#ff0053',
        label: 'Informacion',
        description: <Information />,
        avatar: <Image htmlWidth="48px" src={DELIVERY} />,
    },

    {
        color: '#68cc45',
        label: 'Seguridad',
        description: <Security />,
        avatar: <Image htmlWidth="48px" src={PAYMENTS} />,
    },

    {
        color: '#febe30',
        label: 'Bienvenido!',
        description: <Welcome />,
        avatar: <Image htmlWidth="48px" src={OWNER} />,
    },
];

export default aboutUs;
