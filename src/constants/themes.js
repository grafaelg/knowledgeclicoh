const themes = {
    buy: {
        color: '#68cc45',
        label: 'Compra',
    },

    sell: {
        color: '#ff0037',
        label: 'Venta',
    },
};

export default themes;