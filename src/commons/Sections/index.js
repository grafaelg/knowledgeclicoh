import { Box, Flex, Heading, Stack, Fade } from '@chakra-ui/react';
import ContactForm from '../ContactForm';
import GridAboutUs from '../GridAboutUs';
import GridAverage from '../GridAverage';
import GridCore from '../GridCore';
import ScrollTrigger from 'react-scroll-trigger';
import { useState } from 'react';

const Title = ({ label }) => {
    return (
        <Heading size="3xl" textAlign="center" w="100%">
            {label}
        </Heading>
    );
};

const SectionLayout = ({ children, secondary, id }) => {
    const [show, setShow] = useState(false);
    return (
        <ScrollTrigger onEnter={() => setShow(true)} throttleScroll={5000}>
            {show && <Fade in={true}>
                <Flex
                    id={id}
                    direction="row"
                    flexWrap="wrap"
                    py={{ base: 8, lg: 28 }}
                    px={4}
                    alignContent="center"
                    justifyContent="center"
                    bgColor={secondary ? '#fff' : 'transparent'}
                >
                    <Stack
                        w={{ base: '100%', lg: '5xl' }}
                        max={{ base: '100%', lg: '5xl' }}
                        direction="row"
                        flexWrap="wrap"
                        justifyContent="center"
                        alignContent={secondary ? 'flex-start' : 'stretch'}
                        alignItems={secondary ? 'flex-start' : 'stretch'}
                        spacing={secondary ? 0 : { base: 0, md: 4, lg: 8 }}
                    >
                        {children}
                    </Stack>
                </Flex>
            </Fade>}
        </ScrollTrigger>
    );
};

const Principal = ({ title, component, id }) => {
    return (
        <SectionLayout id={id}>
            <Box w="100%" mb={{ base: 8, lg: 16 }}>
                <Title label={title} />
            </Box>

            {component}
        </SectionLayout>
    );
};

const Secondary = ({ title, component, id }) => {
    return (
        <SectionLayout id={id} secondary>
            <Flex
                bgColor="#f4f8f9"
                w="100%"
                justifyContent="center"
                alignItems="center"
                mb={{ base: 8, lg: 16 }}
                p={8}
                borderRadius="lg"
                shadow="md"
            >
                <Title label={title} />
            </Flex>
            {component}
        </SectionLayout>
    );
};

const Sections = () => {
    return (
        <>
            <Principal
                id="CoreQuotation"
                title="Valores principales"
                component={<GridCore />}
            />
            <Secondary
                id="AverageQuotation"
                title="Valores Promedios Casas de Cambio"
                component={<GridAverage />}
            />
            <Principal
                id="Nosotros"
                title="Todo lo que buscas del dolar."
                component={<GridAboutUs />}
            />
            <Secondary
                id="Contacto"
                title="Contacto"
                component={<ContactForm />}
            />
        </>
    );
};

export default Sections;
