import { HamburgerIcon } from '@chakra-ui/icons';
import {
    Box,
    Text,
    Flex,
    HStack,
    Link,
    Menu,
    MenuList,
    MenuButton,
    MenuItem,
    IconButton,
} from '@chakra-ui/react';

import React from 'react';

const Navigation = () => {
    const items = [
        { label: 'Valores principales', link: 'CoreQuotation' },
        { label: 'Cotización Promedio', link: 'AverageQuotation' },
        { label: 'Nosotros', link: 'Nosotros' },
        { label: 'Contacto', link: 'Contacto' },
    ];

    return (
        <>
            <Menu>

                {items.map((item) => (
                    <Link
                        color="#fff"
                        display={{ base: "none", md: "flex" }}
                        key={`md_${item.link}`}
                        href={`#${item.link}`}
                        fontSize="13px"
                        fontWeight="500"
                    >
                        {item.label}
                    </Link>
                ))}

                <MenuButton
                    as={IconButton}
                    display={{ base: "flex", md: "none" }}
                    aria-label="Options"
                    icon={<HamburgerIcon />}
                    size="xs"
                    variant="outline"
                    bg="#fff"
                />
                <MenuList>
                    {items.map((item) => (
                        <MenuItem key={`base_${item.link}`}>
                            <Link
                                href={`#${item.link}`}
                                fontSize="13px"
                                fontWeight="500"
                            >
                                {item.label}
                            </Link>
                        </MenuItem>
                    ))}
                </MenuList>
            </Menu>
        </>
    );
};

const Logo = () => (
    <Text
        bgGradient="linear(to-l, #F0F0F0, #FFF)"
        bgClip="text"
        fontSize="2xl"
        fontWeight="extrabold"
    >
        Knowledge
    </Text>
);

export const Header = () => {
    return (
        <Flex bg="black" paddingX="8" paddingY="1" justifyContent="center">
            <Flex
                w={{ base: '100%', lg: '5xl' }}
                max={{ base: '100%', lg: '5xl' }}
                justifyContent="space-between"
            >
                <Box p="2">
                    <Logo />
                </Box>

                <HStack
                    spacing="24px"
                    flex="1"
                    alignItems="center"
                    justifyContent="flex-end"
                >
                    <Navigation />
                </HStack>
            </Flex>
        </Flex>
    );
};

export default Header;
