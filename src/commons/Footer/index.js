import { Box, Text, Flex, HStack, Link } from '@chakra-ui/react';

import React from 'react';

const Navigation = () => {
    const items = [
        { label: 'Condiciones de uso', link: '#' },
        { label: 'Aviso legal', link: '#' },
        { label: 'Nosotros', link: '#' },
    ];

    return (
        <>
            {items.map((item, index) => (
                <Link
                    color="#fff"
                    key={item.link}
                    fontSize="13px"
                    fontWeight="500"
                >
                    {item.label}
                </Link>
            ))}
        </>
    );
};

const Copyright = () => (
    <Text
        bgGradient="linear(to-l, #F0F0F0, #FFF)"
        bgClip="text"
        fontSize="md"
        fontWeight="extrabold"
        textAlign={{ base: "center" , md: "right" ,}}
    >
        © Todos los Derechos reservados.
    </Text>
);

export const Footer = () => {
    return (
        <Flex bg="black" paddingX="8" paddingY="1" justifyContent="center">
            <Flex
                w={{ base: '100%', lg: '5xl' }}
                max={{ base: '100%', lg: '5xl' }}
                direction={{ base: 'column', md: 'row' }}
                justifyContent="space-between"
            >
                <HStack
                    spacing="24px"
                    flex="1"
                    justifyContent={{ base: 'center', md: 'flex-start' }}
                    alignItems="center"
                >
                    <Navigation />
                </HStack>

                <Box p="2">
                    <Copyright />
                </Box>
            </Flex>
        </Flex>
    );
};

export default Footer;
