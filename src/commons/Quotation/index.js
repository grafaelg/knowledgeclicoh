import { Text, Flex, Badge } from '@chakra-ui/react';

const Quotation = ({ theme, value }) => (
    <Flex
        direction="row"
        flexWrap="wrap"
        justifyContent="center"
        alignContent="center"
    >
        <Badge
            textTransform="uppercase"
            w="100%"
            textAlign="center"
            fontWeight="700"
            fontSize={{ base: 'xs', lg: 'sm' }}
            mb="2"
            color={theme.color}
        >
            {theme.label}
        </Badge>

        <Text
            w="100%"
            fontSize={{ base: '12px', lg: '18px' }}
            fontWeight="800"
            textAlign="center"
        >
            $ {value}
        </Text>
    </Flex>
);

export default Quotation;
