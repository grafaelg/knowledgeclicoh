import React from 'react';
import { Text, Flex, Stack, VStack } from '@chakra-ui/react';

import aboutUs from '../../constants/aboutUs';

const GridAboutUs = () => (
    <Flex flexDirection="row" flexWrap="wrap" w="100%">
        {aboutUs.map((data, index) => (
            <Flex
                pr={{ base: 0, md: index % 2 === 0 ? 8 : '0px' }}
                w={{ base: '100%', md: '50%' }}
                maxW={{ base: '100%', md: '50%' }}
                mb={8}
                justifyContent="center"
                key={data.label}
            >
                <Flex
                    w="100%"
                    maxW="100%"
                    borderRadius="lg"
                    overflow="hidden"
                    bg="#fff"
                    p={8}
                    flexDirection="column"
                    justifyContent="space-between"
                >
                    <VStack
                        px="2"
                        direction="column"
                        spacing={2}
                        alignItems="flex-start"
                    >
                        {data.avatar}
                        <Text
                            color="#313131"
                            fontWeight="600"
                            letterSpacing=".5px"
                            fontSize="12px"
                            textTransform="uppercase"
                        >
                            {data.label}
                        </Text>
                    </VStack>

                    <Stack
                        direction="row"
                        justifyContent="center"
                        spacing={8}
                        p="4"
                        flex="1"
                    >
                        <Text
                            color="#313131"
                            fontWeight="900"
                            letterSpacing=".5px"
                            fontSize={{ base: "3xl", lg: "5xl" }}
                        >
                            {data.description}
                        </Text>
                    </Stack>
                </Flex>
            </Flex>
        ))}
    </Flex>
);

export default GridAboutUs;
