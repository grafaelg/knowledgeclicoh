import React from 'react';
import { Text, Flex, Box, Input, Textarea } from '@chakra-ui/react';

const TextInput = ({ label }) => {
    return (
        <Box mb={4} w={{base: "100%" , lg: "25%" }} pr={4}>
            <Text
                color="#313131"
                fontWeight="600"
                letterSpacing=".5px"
                fontSize="12px"
                textTransform="uppercase"
                mb={2}
            >
                {label}
            </Text>
            <Input placeholder={label} />
        </Box>
    );
};

const PlaceHolderInput = ({ label }) => {
    return (
        <Box w={{base: "100%" , lg: "50%" }}>
            <Text
                color="#313131"
                fontWeight="600"
                letterSpacing=".5px"
                fontSize="12px"
                textTransform="uppercase"
                mb={2}
            >
                {label}
            </Text>
            <Textarea placeholder={label} />
        </Box>
    );
};

const ContactForm = () => (
    <Flex
        w="100%"
        maxW="100%"
        borderRadius="lg"
        overflow="hidden"
        p={8}
        mb={8}
        shadow="md"
        bg="#f4f8f9"
        flexDirection="row"
        flexWrap="wrap"
        justifyContent="space-between"
    >
        <TextInput label="Nombre" />
        <TextInput label="Correo electronico" />
        <PlaceHolderInput label="Mensaje" />
    </Flex>
);

export default ContactForm;
