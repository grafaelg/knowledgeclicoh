import React from 'react';
import { ContextApp } from '../../library';
import { Text, Flex, Stack, VStack } from '@chakra-ui/react';
import Quotation from '../Quotation';
import themes from '../../constants/themes';
import Variation from './Variation';

const GridCore = () => (
    <ContextApp.Consumer>
        {(context) =>
            context.coreValues.map((data) => (
                <Flex
                    w={{ base: '100%', md: "30%" , lg: '25%' }}
                    maxW={{ base: '100%', md: "30%" , lg: '25%' }}
                    borderRadius="lg"
                    overflow="hidden"
                    bg="#fff"
                    py={8}
                    mb={8}
                    shadow="md"
                    flexDirection="column"
                    justifyContent="space-between"
                >
                    <VStack
                        px="2"
                        direction="column"
                        justifyContent="center"
                        alignItems="center"
                        spacing={2}
                    >
                        {data.avatar}
                        <Text
                            color="#313131"
                            fontWeight="900"
                            letterSpacing=".5px"
                            fontSize="16px"
                            textAlign="center"
                        >
                            {data.nombre}
                        </Text>
                    </VStack>

                    <Stack
                        direction="row"
                        justifyContent="center"
                        spacing={8}
                        p="4"
                        flex="1"
                    >
                        {(data.compra || !isNaN(data.compra)) && (
                            <Quotation theme={themes.buy} value={data.compra} />
                        )}

                        {(data.venta || !isNaN(data.venta)) && (
                            <Quotation theme={themes.sell} value={data.venta} />
                        )}
                    </Stack>

                    {data.variacion && <Variation value={data.variacion} />}
                </Flex>
            ))
        }
    </ContextApp.Consumer>
);

export default GridCore;
