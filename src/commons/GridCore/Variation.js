import { Text, Flex } from '@chakra-ui/react';
import {
    ChevronUpIcon,
    ChevronDownIcon,
    ArrowUpDownIcon,
} from '@chakra-ui/icons';

const Variation = ({ value }) => {  
    return (
        <Flex p="2" justifyContent="center">
            <Text color="#000" fontWeight="700" fontSize="15px">
                {value > 0 && <ChevronUpIcon boxSize="24px"	color="#68cc45" />}
                {value < 0 && <ChevronDownIcon boxSize="24px"	color="#ff0037" />}
                {value === "0" && <ArrowUpDownIcon mr="2" />}

                VARIACIÓN {value} %
            </Text>
        </Flex>
    );
};

export default Variation;
