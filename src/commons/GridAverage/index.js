import React from 'react';
import { ContextApp } from '../../library';
import { Text, Flex, Stack, VStack } from '@chakra-ui/react';
import Quotation from '../Quotation';
import themes from '../../constants/themes';

const Country = ({ name, flag }) => {
    return (
        <VStack
            px="2"
            direction="column"
            justifyContent="center"
            alignItems="center"
            spacing={2}
            w={{ base: '120px', lg: '150px' }}
            h={{ base: '120px', lg: '150px' }}
            borderRadius="full"
            bg="#fff"
        >
            {flag}
            <Text
                color="#313131"
                fontWeight="900"
                letterSpacing=".5px"
                fontSize="12px"
                textAlign="center"
            >
                {name}
            </Text>
        </VStack>
    );
};

const Values = ({ buy, sell }) => {
    return (
        <Stack
            direction="row"
            justifyContent="center"
            spacing={{ base: 4, lg: 8 }}
            p="4"
            flex="1"
        >
            {(buy || !isNaN(buy)) && (
                <Quotation theme={themes.buy} value={buy} />
            )}

            {(sell || !isNaN(sell)) && (
                <Quotation theme={themes.sell} value={sell} />
            )}
        </Stack>
    );
};

const GridAverage = () => (
    <Flex flexDirection="row" flexWrap="wrap" w="100%">
        <ContextApp.Consumer>
            {(context) =>
                context.averageValues.map((data, index) => (
                    <Flex
                        pr={{ base: 0, md: index % 2 === 0 ? 8 : '0px' }}
                        w={{ base: '100%', md: '50%' }}
                        maxW={{ base: '100%', lg: '50%' }}
                        mb={8}
                        justifyContent="center"
                        alignItems="center"
                    >
                        <Flex
                            w="100%"
                            maxW="100%"
                            borderRadius="lg"
                            overflow="hidden"
                            bg="#f4f8f9"
                            p={{ base: 4, lg: 8 }}
                            flexDirection="row"
                            justifyContent="space-between"
                            shadow="md"
                        >
                            <Country name={data.nombre} flag={data.flag} />
                            <Values buy={data.compra} sell={data.venta} />
                        </Flex>
                    </Flex>
                ))
            }
        </ContextApp.Consumer>
    </Flex>
);

export default GridAverage;
