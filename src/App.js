import React, { Component } from 'react';
import { averageValues, ContextApp, coreValues } from "./library"
import Footer from './commons/Footer';
import Header from './commons/Header';
import Sections from './commons/Sections';
import './index.css';

import { ChakraProvider, extendTheme } from '@chakra-ui/react';
import Fonts from './globals/Fonts';


const theme = extendTheme({
    fonts: {
        heading: "'Inter', sans-serif",
        body: "'Inter', sans-serif",
    },
});

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            averageValues: [],
            coreValues: [],
            loading: true
        };
    }

    componentDidMount = async () => {
        try {
            this.setState({ loading: true })
            const promises = await Promise.all([coreValues(), averageValues()]);
            const core = promises[0]
            const average = promises[1]
            this.setState({ coreValues: core, averageValues: average, loading: false })
        } catch (err) {
            console.error(err)
        }
    }

    render() {
        const { loading } = this.state
        if (loading) return <></>
        return (
            <ContextApp.Provider value={this.state}>
                <ChakraProvider theme={theme}>
                    <Fonts />
                    <Header />
                    <Sections />
                    <Footer />
                </ChakraProvider>
            </ContextApp.Provider>
        );
    }
}
