import { getData, transformToNumber } from './utils';
import { Image } from '@chakra-ui/react';

import DOLAR_BLUE from '../assets/image/dolarblue.svg';
import DOLAR_BOLSA from '../assets/image/dolarbolsa.svg';
import DOLAR_CONTADO from '../assets/image/dolarcontado.svg';
import DOLAR_OFICIAL from '../assets/image/dolaroficial.svg';
import DOLAR_TURISTA from '../assets/image/dolarturista.svg';
import DOLAR_PROMEDIO from '../assets/image/dolarpromedio.svg';

const URL = 'https://www.dolarsi.com/api/api.php?type=valoresprincipales';

const CORE_VALUES = [
    {
        name: 'Dolar Oficial',
        avatar: <Image htmlWidth="32px" src={DOLAR_OFICIAL} />,
    },
    {
        name: 'Dolar Blue',
        avatar: <Image htmlWidth="32px" src={DOLAR_BLUE} />,
    },
    {
        name: 'Dolar Contado con Liqui',
        avatar: <Image htmlWidth="32px" src={DOLAR_CONTADO} />,
    },
    {
        name: 'Dolar',
        avatar: <Image htmlWidth="32px" src={DOLAR_PROMEDIO} />,
    },
    {
        name: 'Dolar Bolsa',
        avatar: <Image htmlWidth="32px" src={DOLAR_BOLSA} />,
    },
    {
        name: 'Dolar turista',
        avatar: <Image htmlWidth="32px" src={DOLAR_TURISTA} />,
    },
];

const coreValues = async () => {
    let data = await getData(URL);

    data = data.map((value) => {
        const hasAvatar = CORE_VALUES.find((i) => i.name === value.nombre);
        if (!hasAvatar) return false;

        return {
            ...value,
            avatar: hasAvatar.avatar,
        };
    });

    data = data.filter((value) => !!value);
    data = data.map((value) => {
        value.venta = transformToNumber(value.venta);
        value.compra = transformToNumber(value.compra);
        value.variacion = transformToNumber(value.variacion);
        return value;
    });

    return data;
};

export default coreValues;
