import React from 'react';
import { Image } from '@chakra-ui/react';
import { getData } from './utils';

import USA from '../assets/image/flag/usa.svg';
import PGY from '../assets/image/flag/pgy.svg';
import BRZ from '../assets/image/flag/brz.svg';
import URU from '../assets/image/flag/uru.svg';
import UER from '../assets/image/flag/uer.svg';
import ENG from '../assets/image/flag/eng.svg';

const URL = 'https://www.dolarsi.com/api/api.php?type=cotizador';

const AVERAGE_VALUES = [
    { name: 'Dolar', flag: <Image htmlWidth="32px" src={USA} /> },
    { name: 'Euro', flag: <Image htmlWidth="32px" src={UER} /> },
    { name: 'Real', flag: <Image htmlWidth="32px" src={BRZ} /> },
    { name: 'Libra Esterlina', flag: <Image htmlWidth="32px" src={ENG} /> },
    { name: 'Peso Uruguayo', flag: <Image htmlWidth="32px" src={URU} /> },
    { name: 'Guaraní', flag: <Image htmlWidth="32px" src={PGY} /> },
];

const averageValues = async () => {
    let data = await getData(URL);

    data = data.map((value) => {
        const hasFlag = AVERAGE_VALUES.find((i) => i.name === value.nombre);
        if (!hasFlag) return;

        return {
            ...value,
            flag: hasFlag.flag,
        };

    });

    data = data.filter((value) => !!value)

    return data;
};

export default averageValues;
