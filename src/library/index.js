import { createContext } from 'react';
import coreValues from './coreValues';
import averageValues from './averageValues';

export const ContextApp = createContext({
    coreValues: [],
    averageValues: [],
});

export { coreValues, averageValues };
