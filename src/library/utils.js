export const getData = async (url) => {
    const promise = await fetch(url, {
        method: 'GET',
    });
    let data = await promise.json();
    data = data.map((value) => value.casa);
    return data;
};

export const transformToNumber = (value) => {
    if (!value) return
    if (value === "0") return "0"
    return parseFloat(value.replace(",", "."))
}